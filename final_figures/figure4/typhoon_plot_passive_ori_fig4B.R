#/usr/local/bin/R

library(Rsamtools)
library(GenomicRanges)

##call functions
source('~/create_typhoon_plot_functions_sacCer3genetablemodified_xlimmodified.R')
source('~/plot_nucleosome_functions.R')
source('~/process_bam_functions.R')
source('~/create_typhoon_plot_functions.R')

#bam file name
file_name = c( "nascent","mature")

dir_path <- "~/path-to-bam-file/"

#individual plot title names, in same order as in the file_name
plot_title_1 = "Nascent"
plot_title_2 = "Mature"

#define the read boudaries 
length_min = 20
length_max = 250

# this is  to create a list of matrices to be able to call them individually later on.
data = vector("list")

# Set the storage list  for nucleosome schematic
nuc_peaks.l = vector("list")

#for(o in 1:nrow(origenes)){
origins <- read.csv("~/oridb_acs_feature_file_jab-curated-798-sites_sacCer3.csv", stringsAsFactors = F)

ori <- "ARS602"
#setting up parameters
chr = origins[which(origins$ars_name == ori),"chr"]


#p1 = (origenes$end[o]-origenes$start[o])/2
p=origins[which(origins$ars_name == ori),"pos"]

new_start= p - 1000
new_end= p + 1000


#make matrix, a column for each fragment read length                       
matx.m = matrix(0, nrow=250, ncol=(new_end-new_start)+1)
matx_rpkm.m = matrix(0, nrow=250, ncol=(new_end-new_start)+1)

chr.gr = GRanges(seqnames= chr, ranges = IRanges(start =new_start , end = new_end ))

p = ScanBamParam(what = c("rname", "strand", "pos", "isize"),which = chr.gr)

for (f in 1:2){
  
  #data files
  file_name.bam = paste(dir_path,file_name[f],".bam", sep='')
  file_name.bam.bai = paste(dir_path,file_name[f],".bam.bai",sep='')
  
  A_reads.l = scanBam(file = file_name.bam,
                      index = file_name.bam.bai,
                      param = p)
  
  #create a new GenomicRanges object for the reads 
  A_reads.gr = GRanges(seqnames = A_reads.l[[1]]$rname,
                       ranges = IRanges(start = A_reads.l[[1]]$pos,
                                        width = A_reads.l[[1]]$isize))
  
  #define bp coverage.
  mat.gr = GRanges(seqnames = chr, ranges = IRanges(start=new_start:new_end,width=1))
  
  for(i in 20:250){
    subset_data.gr = A_reads.gr[which(width(A_reads.gr)==i)]
    
    start(subset_data.gr) = start(subset_data.gr) + width(subset_data.gr)/4
    end(subset_data.gr) = end(subset_data.gr) - width(subset_data.gr)/4
    
    matx.m[i,]=countOverlaps(mat.gr, subset_data.gr) 
    
  }
  data[[f]] <- matx.m
  
  # Get the feature density
  nuc.v = GetMNaseFeatureDensity(file_name.bam, chr, new_start, new_end, 170, 200, 40)
  
  # Find the nucleosome peaks
  nuc_peaks.l[[f]] = GetDensityPeaks(nuc.v)
  
}

#making the plot
cat("Creating the plot...\n")
library(gplots)
scr.m = matrix(c(0.05, 1, 0.91, 1, 
                 0.05, 1, 0.85, 0.90,
                 0.05, 1, 0.8, 0.85,
                 0.05, 1, 0.45, 0.8,
                 0.05, 1, 0.1, 0.45,
                 
                 0.05, 1, 0, 0.05,
                 0.04, 0.05, 0.1, 0.8),
               ncol = 4, byrow = T
)

my_plot1.s = split.screen(scr.m)

zm=25
range= 1:ncol(data[[1]])
screen(my_plot1.s[1])
par(mar = c(0,2.1,0,2.1), cex=0.7)  
make_gene_schematic(chr, new_start, new_end, proteinCoding = F)

screen(my_plot1.s[2])
par(mar = c(0,2.1,0,2.1))
SetChromatinSchematic(x_start = new_start, x_end = new_end, y_start = 0, y_end = 1)
# Make the nucleosomes
PlotNucleosome(nuc_peaks.l[[1]])

screen(my_plot1.s[3])
par(mar = c(0,2.1,0,2.1))
# Make the nucleosomes
SetChromatinSchematic(x_start = new_start, x_end = new_end, y_start = 0, y_end = 1)
PlotNucleosome(nuc_peaks.l[[2]])

screen(my_plot1.s[4])
par(mar = c(1,2.1,1,2.1), cex=1, cex.main=1)
dens_dot_plot(data[[1]][,range], z_min = 0, z_max = 5, plot_title=plot_title_1, x_axt = "n")
axis(side=1, at= seq(new_start, new_end, 500), labels=F, tck= -0.05)

screen(my_plot1.s[5])
par(mar = c(1,2.1,1,2.1), cex=1, cex.main=1)
dens_dot_plot(data[[2]][,range], z_min = 0, z_max = 5, plot_title=plot_title_2, x_axt = "n")
axis(side=1, at= seq(new_start, new_end, 500), labels= seq(new_start/1000, new_end/1000, 0.5), tck= -0.05)

screen(my_plot1.s[6])
par(mar=c(0,0,0,0))
mtext(paste(chr,'Position (Kb)', sep = ''), side = 1, cex=1)

screen(my_plot1.s[7])
par(mar=c(0,0,0,0))
mtext('Fragment Length (bp)', side = 2, cex=1)



