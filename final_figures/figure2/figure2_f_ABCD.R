# Clear the workspace

graphics.off()
rm(list = ls())

# Load the functions
source('~/create_typhoon_plot_functions_sacCer3genetablemodified_xlimmodified.R')
source("~/mnase_acs_paper_final_figures.function.R")
source('~/create_typhoon_plot_functions.R')

# Set the directory where all the individual panel scripts are 
subpanel_dir = "~/figure2/"

# Set up the path to save the plot
tiff(file ="~/Gutierrez_Fig2.tiff", height = 10, width = 13, units = "in", res = 500, compression = "lzw")

# Setup the screen
overall_scr.m = matrix(c(0, 0.2, 0,1,
                         0.2, 0.7, 0.5, 1, 
                         0.7, 1, 0.5, 1,
                         0.2, 0.6, 0.01, 0.5,
                         0.6, 1, 0.01, 0.5,
                         
                         0, 0.03, 0.95, 1,
                         0.19, 0.22, 0.95, 1,
                         0.68, 0.71, 0.95, 1,
                         0.19, 0.22, 0.49, 0.52,
                         0.58, 0.61, 0.49, 0.52
                        
),
ncol = 4, byrow = T
)

# Split the overall screen
close.screen(all.screens = T)

# Setup the screen
all_plots.s = split.screen(overall_scr.m)
par(oma = c(0, 0, 0, 0)) 

# Panel A:
cat("\tMaking plot A\n")
screen(all_plots.s[1])
par(mai=c(0,0,0,0))

# Panel B:
cat("\tMaking plot B\n")
screen(all_plots.s[2])
par(mai=c(0,0,0,0))
source(paste(subpanel_dir, "site_specific_multiple_typhoon_plot_NUCschematic_Fig2.R", sep = ""))

# Panel C:
cat("\tMaking plot C\n")
screen(all_plots.s[3])
par(mai=c(0,0,0,0))
source(paste(subpanel_dir, "cor_plot_2_2A.R", sep = ""))

# Panel D:
cat("\tMaking plot D\n")
screen(all_plots.s[4])
par(mai=c(0,0,0,0))
source(paste(subpanel_dir, "ACF_boxplot_2D.R", sep = ""))

# Panel E:
cat("\tMaking plot E\n")
screen(all_plots.s[5], new = TRUE)
par(mai=c(5,5,5,5))
source(paste(subpanel_dir, "heatmap_PTMs_image.R", sep = ""))

# Add in the Figure Labels
screen(all_plots.s[6])
par(mai=c(0,0,0,0))
add_figure_label("A")

screen(all_plots.s[7])
par(mai=c(0,0,0,0))
add_figure_label("B")

screen(all_plots.s[8])
par(mai=c(0,0,0,0))
add_figure_label("C")

screen(all_plots.s[9])
par(mai=c(0,0,0,0))
add_figure_label("D")

screen(all_plots.s[10])
par(mai=c(0,0,0,0))
add_figure_label("E")

mtext(expression(bold("Figure_2")), side =1, cex=1.5, outer=T, line = -1, adj = 0)

# Close all the screens
close.screen(all.screens = T)

# Close the device
dev.off()
