library(viridis) #The viridis color palettes


cl <- viridis(100, alpha = 1, begin = 0, end = 1, option = "D")

n=60
m=15

dens.m = matrix(c(0.05, 1, 0.1, 1,
                 0.05, 1, 0.08, 0.1, #xlab
                 0.02, 0.04, 0.1, 1), #ylab
               ncol = 4, byrow = T
               )

dens_plot.s = split.screen(dens.m)

screen(dens_plot.s[1])
par(mar = c(2,2.1,2,2.1), cex=1)
plot(density(genes.df$M_B_corr,na.rm = T), col=cl[m],  xlim=c(0.2,1.05),
     xlab='', lwd="3",
     ylab='Density', main='')

lines(density(genes.df$N_B_corr,na.rm = T), col=cl[n], lwd="3")

#t.test(genes.df$M_B_corr,genes.df$N_B_corr)

legend(0.2,6, pt.bg = c(cl[m], cl[n]), col="black", c("Mature", "Nascent"),pch=c(21,21),
       pt.cex=1.25,cex=1.2, bty = "n", y.intersp=1.2, x.intersp=0.5, horiz=F)

screen(dens_plot.s[2])
par(mar=c(0,0,0,0))
mtext('Correlation with Bulk Chromatin', side = 1, cex=1)

screen(dens_plot.s[3])
par(mar=c(0,0,0,0))
mtext('Density Distribution', side = 2, cex=1)

