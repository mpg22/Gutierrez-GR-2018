#/usr/local/bin/R

library(Rsamtools)
library(GenomicRanges)

#bam file name
file_name = c( "early_origin_EdU_labeled")

dir_path <- "~/your-directory-path/"

options(scipen=999)
chr_coordinates.df <- data.frame(chr=(c("chrI", "chrII", "chrIII", "chrIV", "chrV", "chrVI", "chrVII", "chrVIII", "chrIX", "chrX", "chrXI", "chrXII",
                                        "chrXIII", "chrXIV", "chrXV","chrXVI")),
                                 end=as.numeric(c("230218","813184", "316620", "1531933", "576874", "270161", "1090940", "562643", "439888", "745751", "666816", "1078177", "924431", "784333", "1091291", "948066")), stringsAsFactors=FALSE)


# this is  to create a list of matrices that contains all of them, allows to call them individually later on. 
data = vector("list")

  chr = "chrIV"
  
  #chromosome length (start and end) for GenomicRanges
  new_start= 1
  new_end= chr_coordinates.df$end[4] 
  
  chr.gr = GRanges(seqnames= chr, ranges = IRanges(start =new_start , end = new_end ))
  
  p = ScanBamParam(what = c("rname", "strand", "pos", "isize"),which = chr.gr)
  
    
  #data files
  file_name.bam = paste(dir_path,file_name,".bam", sep='')
  file_name.bam.bai = paste(dir_path,file_name,".bam.bai",sep='')
  
  A_reads.l = scanBam(file = file_name.bam, 
                      index = file_name.bam.bai,
                      param = p)                             
    
  #create a new GenomicRanges object for the reads
  A_reads.gr = GRanges(seqnames = A_reads.l[[1]]$rname,
                       ranges = IRanges(start = A_reads.l[[1]]$pos, 
                                        width = A_reads.l[[1]]$isize))
    
  #The coverage method quantifies the degree of overlap for all the ranges in a GRanges object. 
  cov <- coverage(A_reads.gr)
    
  #expand from the "coverage" output. 
  rpts.v <- rep(runValue(cov[[chr]]),runLength(cov[[chr]])) # repeats. 
    
  
  par(mar = c(1,2.1,1,2.1), cex=1, cex.main=1)
  plot( rpts.v, ylim= c(0,7000), type="h", lwd=1, bty="n",
       col="blue",xlab="", xaxt='n', yaxt='n',ylab="") 
  
  axis(1, at=seq(1, new_end - new_start, length.out=10), 
       labels=as.integer(seq(new_start/1000, new_end/1000, length.out=10)))
  axis(2, at=seq(0, 12711, length.out=10), 
       labels=NA , tick = 0.5)
  
 mtext('Coverage', side = 2, outer = F, cex=1, line = 2)
   

  


