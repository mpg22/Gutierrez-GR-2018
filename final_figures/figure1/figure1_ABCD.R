# Clear the workspace
graphics.off()
rm(list = ls())

# Load the functions
source('~/create_typhoon_plot_functions_sacCer3genetablemodified_xlimmodified.R')
source('~/mnase_acs_paper_final_figures.function.R')

# Set the directory to save the plot
subpanel_dir = "/data/home/mpg22/MANUSCRIPT/figures_final/figure1/"

tiff(file ="~/Gutierrez_Fig1.tiff",height = 10, width = 14, units = "in", res = 500, compression = "lzw")

# Setup the screen
overall_scr.m = matrix(c(0, 1, 0.8, 1, #left, right, bottom, top (x1, x2, y1, y2)
                         0.05, 1, 0.67, 0.80,
                         0, 1, 0.43, 0.63,
                         
                        0.035, 0.35, 0.05, 0.4,
                        0.36, 0.68, 0.05, 0.4,
                        0.69, 1, 0.05, 0.4,
                        
                         0, 0.02, 0.95, 1,
                         0, 0.02, 0.38, 0.41,
                         0.34, 0.37, 0.38, 0.41,
                         0.67, 0.70, 0.38, 0.41
),
ncol = 4, byrow = T
)

# Split the overall screen
close.screen(all.screens = T)

# Setup the screen
all_plots.s = split.screen(overall_scr.m)
par(oma = c(1, 1, 1, 1)) # make room (i.e. the 4's) for the overall x and y axis titles
#bottom, left, top, right

# Panel A:
cat("\tMaking plot A.1\n")
screen(all_plots.s[1])
par(mai=c(0,0,0,0))
source(paste(subpanel_dir, "single_typhoon_plot_figA1.R", sep = ""))

cat("\tMaking plot A.2\n")
screen(all_plots.s[2])
par(mai=c(0,0,0,0))
source(paste(subpanel_dir, "coverage_of_sync_figA2.R", sep = ""))

cat("\tMaking plot A.3\n")
screen(all_plots.s[3])
par(mai=c(0,0,0,0))
source(paste(subpanel_dir, "single_typhoon_plot_figA3.R", sep = ""))

# Panel B:
cat("\tMaking plot B\n")
screen(all_plots.s[4])
par(mai=c(0,0,0,0))
source(paste(subpanel_dir, "site_specific_multiple_typhoon_plot_ordered_gene_fig1C.R", sep = ""))

# Panel C:
cat("\tMaking plot C\n")
screen(all_plots.s[5])
par(mai=c(0,0,0,0))
source(paste(subpanel_dir, "site_specific_multiple_typhoon_plot_DISordered_gene_fig1D.R", sep = ""))

# Panel D:
cat("\tMaking plot D\n")
screen(all_plots.s[6])
par(mai=c(0,0,0,0))
source(paste(subpanel_dir, "site_specific_multiple_typhoon_plot_RAD51_gene_fig1E.R", sep = ""))

# Add in the Figure Labels
screen(all_plots.s[7])
par(mai=c(0,0,0,0))
add_figure_label("A")

screen(all_plots.s[8])
par(mai=c(0,0,0,0))
add_figure_label("B")

screen(all_plots.s[9])
par(mai=c(0,0,0,0))
add_figure_label("C")

screen(all_plots.s[10])
par(mai=c(0,0,0,0))
add_figure_label("D")

mtext(expression(bold("Figure_1")), side =1, cex=1.5, outer=T, line = 0, adj = 0)

# Close all the screens
close.screen(all.screens = T)

# Close the device
dev.off()
