cl <- viridis(100, alpha = 1, begin = 0, end = 1, option = "C") #C
n=  5
int = "dark grey" #cl[5]
m=  50


lg.m = matrix(c(0, 1, 0.49, 0.9, #(x1, x2, y1, y2)
                 0, 1, 0.01, 0.49),
               ncol = 4, byrow = T)


lg_plot.s = split.screen(lg.m)

screen(lg_plot.s[1])

par(mar = c(0,0,0,0)) # changing the margings bottom, left, top, and right.
plot(0, 0, type = "n", bty = "n", xaxt = "n", yaxt = "n")

legtext <- c("Transient", "Slow", "Fast")
xcoords <- c(0,0.5,0.5)
secondvector <- (1:length(legtext))-1
textwidths <- xcoords/secondvector # this works for all but the first element
textwidths[1] <- 0 # so replace element 1 with a finite number (any will do)

legend("top" ,legend=legtext,  pt.bg =c(cl[n],cl[m],int), 
       pch=c(21,21,21), 
       col="black",text.width=textwidths,
       pt.cex=1.5,cex=0.9,bty = "n", h=T, y.intersp=1.25, x.intersp=1)

screen(lg_plot.s[2])
par(mar = c(0,0,0,1)) # changing the margings 
plot(0, 0, type = "n", bty = "n", xaxt = "n", yaxt = "n")
legend("top", col="black", 
       c("Nascent Chromatin", "Mature Chromatin"),
       lty = c( "dotted", "solid"),
       lwd = c(1.5,1.5),
       cex=0.9,bty = "n", h=T, y.intersp=1.25, x.intersp=1)

#legend("topright", col="black", 
 #      c("Top 10th Percentile ", "Bottom 10th Percentile","unchanged" ,"Nascent", "Mature"),
  #     pch=c(21,21,21, NA, NA), lty = c(NA, NA, NA, "dotted", "solid"),
   #    lwd = c(NA, NA, NA, 3,3),pt.bg = c("black", "red", "blue","dark grey", "dark grey"),
    #   pt.cex=1,cex=1.5, bty = "n", h=T, y.intersp=1.5, x.intersp=1)

