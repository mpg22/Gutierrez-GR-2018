library(viridis) #The viridis color palettes

#import transcription factor data frame from MacIsaac et al,2006 with occupancy scores in mature chromatin 
tfs_mac <- read.csv("~/tfs_from_MacIsaac_june2018.csv", stringsAsFactors = F)

# find the top 10th percentile of occupancy in mature chromatin
q <- quantile(tfs_mac$mat_reads, seq(0,1,.10))
tf <- tfs_mac[which(tfs_mac$mat_reads > q[10]), ]

#re-define the range from the TF position at which we want to capture nucleosome information 
for(i in 1:nrow(tf)){
  tf$nstart[i] <- tf$pos[i] - 550
  tf$nend[i] <- tf$pos[i] + 550
}

for(i in 1:nrow(tf)){
  
  cr_tf <- tf$chr[i]
  
  #subset from the nucleosome master data frame
  cr_nucs <- chr_nuc_ratios.df[which(chr_nuc_ratios.df$chr == cr_tf),]
  
  idx <- which(cr_nucs$nuc_peaks > tf$nstart[i] & cr_nucs$nuc_peaks < tf$nend[i])
  osi <- cr_nucs$nuc_peaks[idx]
  osi <- c(osi, tf$pos[i])
  osio <- osi[order(osi, decreasing = F)]
  
  z <- which(osio == tf$pos[i] )
  
  f <-  c(osio[z-1], osio[z+1]) # first nucleosome pair
  s <-  c(osio[z-2], osio[z+2]) # second nucleosome pair
  t <-  c(osio[z-3], osio[z+3]) # third nucleosome pair
  
  if(is.na(mean(t)) == T){
    next()
  }
  
  if(length(which(is.na(t) == T)) > 0){
    d <- which(is.na(t) == F)
    t1 <- c(t[d],t[d])
  } else {
    t1 <- t
  }
  
  
  if(length(s) == 1){
    s1 <- c(s,s)
  } else {
    s1 <- s
  }
  
  if(length(t) == 1){
    t1 <- c(t,t)
  }
  
  
  tf$N1[i] <- mean(cr_nucs$Nnuc_mdpAv[which(cr_nucs$nuc_peaks == f[1])], cr_nucs$Nnuc_mdpAv[which(cr_nucs$nuc_peaks == f[2])])
  tf$M1[i] <- mean(cr_nucs$Mnuc_mdpAv.[which(cr_nucs$nuc_peaks == f[1])], cr_nucs$Mnuc_mdpAv.[which(cr_nucs$nuc_peaks == f[2])])
  
  tf$N2[i] <- mean(cr_nucs$Nnuc_mdpAv[which(cr_nucs$nuc_peaks == s1[1])], cr_nucs$Nnuc_mdpAv[which(cr_nucs$nuc_peaks == s1[2])])
  tf$M2[i] <- mean(cr_nucs$Mnuc_mdpAv.[which(cr_nucs$nuc_peaks == s1[1])], cr_nucs$Mnuc_mdpAv.[which(cr_nucs$nuc_peaks == s1[2])])
  
  tf$N3[i] <- mean(cr_nucs$Nnuc_mdpAv[which(cr_nucs$nuc_peaks == t1[1])], cr_nucs$Nnuc_mdpAv[which(cr_nucs$nuc_peaks == t1[2])])
  tf$M3[i] <- mean(cr_nucs$Mnuc_mdpAv.[which(cr_nucs$nuc_peaks == t1[1])], cr_nucs$Mnuc_mdpAv.[which(cr_nucs$nuc_peaks == t1[2])])
  
}

options(repr.plot.width=5, repr.plot.height=5)

cl <- viridis(100, alpha = 1, begin = 0, end = 1, option = "D")
n=60
m=15


reg.m = matrix(c(0.05, 1, 0.1, 1,
                  0.05, 1, 0.08, 0.1, #xlab
                  0.02, 0.04, 0.1, 1), #ylab
                ncol = 4, byrow = T
)


reg_plot.s = split.screen(reg.m)

screen(reg_plot.s[1])
par(mar = c(2,2.1,2,2.1), cex=1)

boxplot(tf[,c(13,12,15,14,17,16)],notch=TRUE,outline=FALSE, ylab="Mean Midpoint Distance", main="",
        names =c("M1", "N1", "M2", "N2", "M3", "N3"), ylim=c(0,58))

stripchart(tf[,c(13,12,15,14,17,16)],vertical = TRUE, method = "jitter", add = TRUE, pch = 20, cex=0.25,
           col = alpha(c(cl[m],cl[n]), 0.5))

t.test(tf[,14], tf[,16])

Ml = data.frame(x = c(1,3,5), y = sapply(tf[,c(13,15,17)], median, na.rm=T))
Nl = data.frame(x = c(2,4,6), y = sapply(tf[,c(12,14,16)], median, na.rm=T))

m_lm <- lm(y ~ x, data=Ml[1:3,])
n_lm <- lm(y ~ x, data=Nl[1:3,])

abline(m_lm, col=cl[m], lwd=3)
abline(n_lm, col=cl[n], lwd=3)

m_rsq <- round(summary(m_lm)$r.squared, 2)
n_rsq <- round(summary(n_lm)$r.squared, 2)

legend("topleft",pt.bg = c(cl[n], cl[m]), c(paste("Nascent R-sq", n_rsq, sep=' '),
                                      paste("Mature R-sq",  m_rsq, sep=' ')),
       pch=c(21,21), pt.cex=1.5,cex=1, col="black",
       bty = "n", y.intersp=1.25, x.intersp=1, horiz = F)

t.test(tf[,12], tf[,13])

####ADDDING STATISTICS 
# capture x coordinates of bars
x <- c(1,2,3,4, 5, 6)
# create the y coordinate of the line
y <- 41.5
# set an offset for tick lengths
offset <- 1.5
# draw first horizontal line
lines(x[1:2],c(y, y), lwd=2)
# draw ticks
lines(x[c(1,1)],c(y, y-offset), lwd=2)
lines(x[c(2,2)],c(y, y-offset), lwd=2)
# draw asterics
text(x[1]+((x[2]-x[1])/2),y+offset,"n.s.", cex=1)


# create the y coordinate of the line
y <- 39
# draw first horizontal line
lines(x[3:4],c(y, y), lwd=2)
# draw ticks
lines(x[c(3,3)],c(y, y-offset), lwd=2)
lines(x[c(4,4)],c(y, y-offset), lwd=2)
# draw asterics
text(x[3]+((x[4]-x[3])/2),y+offset,"****", cex=1.5)

# create the y coordinate of the line
y <- 40
# draw first horizontal line
lines(x[5:6],c(y, y), lwd=2)
# draw ticks
lines(x[c(5,5)],c(y, y-offset), lwd=2)
lines(x[c(6,6)],c(y, y-offset), lwd=2)
# draw asterics
text(x[5]+((x[6]-x[5])/2),y+offset,"****", cex=1.5)

# create the y coordinate of the line
y <- 43
# draw first horizontal line
lines(x[c(3,5)],c(y, y), lwd=2)
# draw ticks
lines(x[c(3,3)],c(y, y-offset), lwd=2)
lines(x[c(5,5)],c(y, y-offset), lwd=2)
# draw asterics
text(x[5]+((x[3]-x[5])/2),y+offset,"n.s.", cex=1)

# create the y coordinate of the line
y <- 47.5
# draw first horizontal line
lines(x[c(4,6)],c(y, y), lwd=2)
# draw ticks
lines(x[c(4,4)],c(y, y-offset), lwd=2)
lines(x[c(6,6)],c(y, y-offset), lwd=2)
# draw asterics
text(x[6]+((x[4]-x[6])/2),y+offset,"****", cex=1.5)

#Symbol Meaning
#ns P > 0.05
#* P ≤ 0.05
#** P ≤ 0.01
#*** P ≤ 0.001
#**** P ≤ 0.0001 (For the last two choices only)


screen(reg_plot.s[2])
par(mar=c(0,0,0,0))
mtext('Pairs of Flanking Nucleosomes', side = 1, cex=1)

screen(reg_plot.s[3])
par(mar=c(0,0,0,0))
mtext('Mean Positioning Score', side = 2, cex=1)

