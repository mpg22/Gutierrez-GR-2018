
rm(list = ls())


library(Rsamtools)
library(GenomicRanges)

#settings 
file_name <- c('htz1','h2ak5ac','h3k14ac','h3k23ac','h3k27ac','h3k36me2',
               'h3k36me','h3k4ac','h3k4me2','h3k4me','h3k56ac','h3k79me',
               'h3k9ac','h3s10ph','h4k12ac','h4k16ac','h4k5ac','h4k8ac',
               'h4r3me2s','h4r3me','h3k36me3','h3k18ac','h3k4me3',
               'h4k20me','h3k79me3','h2as129ph')

dir_path <- "~/your-directory-path/"

  
for(f in 1:26){
  
#data files
  file_name.bam = paste(dir_path,file_name[f],".bam", sep='')
  file_name.bam.bai = paste(dir_path,file_name[f],".bam.bai",sep='')
  

# finding total number of reads. 
p1 = ScanBamParam(what = c("rname", "strand", "pos", "qwidth"))

T_reads.l = scanBam(file = file_name.bam,
                    index = file_name.bam.bai,
                    param = p1)


#create a new GenomicRanges object to determine the total number of reads in the bam file
T_reads.df = as.data.frame(GRanges(seqnames = T_reads.l[[1]]$rname,
                                   ranges = IRanges(start = T_reads.l[[1]]$pos, 
                                                    width = T_reads.l[[1]]$qwidth),
                                   strand = T_reads.l[[1]]$strand))

total_reads <- nrow(T_reads.df[-(which(T_reads.df$seqnames == "chrM")),]) # eliminate any information in chrM

for (m in 1:nrow(genes.df)){
  
  new_start= (genes.df[m, "start"])
  new_end= (genes.df[m, "end"])
  
  chr.gr = GRanges(seqnames= chr, ranges = IRanges(start =new_start , end = new_end ))
  
  p = ScanBamParam(what = c("rname", "strand", "pos", "qwidth"),which = chr.gr)
  
  A_reads.l = scanBam(file = file_name.bam,
                      index = file_name.bam.bai,
                      param = p)
  
  #create a new GenomicRanges object for the reads from this list:
  A_reads.gr = GRanges(seqnames = A_reads.l[[1]]$rname,
                       ranges = IRanges(start = A_reads.l[[1]]$pos, 
                                        width = A_reads.l[[1]]$qwidth),
                       strand = A_reads.l[[1]]$strand)
  
  #taking only the midpoint                         
  ranges(A_reads.gr) = IRanges(start=mid(ranges(A_reads.gr)), width=1)
  
  ss_data.df <- as.data.frame(A_reads.gr)
  
  #RPKM normalization
  C <-  nrow(ss_data.df) #Number of reads mapped to a gene
  N <-  as.numeric(total_reads) #Total mapped reads in the experiment
  L <-  genes.df$end[m] - genes.df$start[m] #length in base-pairs for a gene
  
  genes.df[m, file_name[f]] <- (10^9 * C)/(N * L)
  
  cat("\t done with counts in gene",m ,"\n")

  }
}

